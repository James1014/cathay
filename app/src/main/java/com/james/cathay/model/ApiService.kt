package com.james.cathay.model

import com.james.cathay.model.api.ApiResponseItem
import com.james.cathay.model.api.Result
import com.james.cathay.model.vo.ExhibitionCenterItem
import com.james.cathay.model.vo.PlantItem
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("opendata/datalist/apiAccess")
    suspend fun getExhibitionCenter(
        @Query("q") q: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("scope") scope: String,
        @Query("rid") rid: String
    ): Response<ApiResponseItem<Result<ExhibitionCenterItem>>>

    @GET("opendata/datalist/apiAccess")
    suspend fun getPlanet(
        @Query("q") q: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("scope") scope: String,
        @Query("rid") rid: String
    ): Response<ApiResponseItem<Result<PlantItem>>>
}