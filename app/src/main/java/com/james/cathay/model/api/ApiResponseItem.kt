package com.james.cathay.model.api

import com.google.gson.annotations.SerializedName

data class ApiResponseItem<T>(
    @SerializedName("result")
    val result: T?
)