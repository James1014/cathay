package com.james.cathay.model.api

import com.google.gson.annotations.SerializedName

data class Result<T>(
    @SerializedName("limit")
    val limit: Int?,

    @SerializedName("offset")
    val offset: Int?,

    @SerializedName("count")
    val count: Int?,

    @SerializedName("sort")
    val sort: String?,

    @SerializedName("results")
    val results: List<T>?
)