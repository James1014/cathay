package com.james.cathay.model

class ApiRepository(private val apiService: ApiService) {

    // todo: for demo...
    companion object {
        const val SCOPE = "resourceAquire"
    }

    suspend fun getExhibitionCenter(
        rid: String
    ) = apiService.getExhibitionCenter("",0, 0, SCOPE, rid)

    suspend fun getPlanet(
        rid: String
    ) = apiService.getPlanet("",0, 0, SCOPE, rid)

}