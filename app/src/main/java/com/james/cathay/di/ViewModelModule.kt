package com.james.cathay.di

import com.james.cathay.view.detail.DetailViewModel
import com.james.cathay.view.info.InfoViewModel
import com.james.cathay.view.home.HomeViewModel
import com.james.cathay.view.main.MainViewModel
import com.james.cathay.view.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel() }
    viewModel { SplashViewModel() }
    viewModel { HomeViewModel() }
    viewModel { InfoViewModel() }
    viewModel { DetailViewModel() }
}