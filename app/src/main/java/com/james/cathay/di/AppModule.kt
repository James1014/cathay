package com.james.cathay.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module

val appModule = module {
    single { provideGson() }
}

fun provideGson(): Gson {
    return GsonBuilder().create()
}