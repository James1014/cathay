package com.james.cathay.view.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.james.cathay.R
import com.james.cathay.model.vo.ExhibitionCenterItem
import com.james.cathay.view.base.BaseFragment
import com.james.cathay.view.info.InfoFragment
import com.james.cathay.view.listener.OnItemClickListener
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.layout_title.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class HomeFragment : BaseFragment<HomeViewModel>() {
    private val viewModel by viewModel<HomeViewModel>()

    private lateinit var adapter: ExhibitionCenterAdapter

    override fun fetchViewModel(): HomeViewModel? { return viewModel }

    override fun getLayoutId(): Int { return R.layout.fragment_home }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSettings()
    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.data.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                tv_no_data.visibility = View.GONE
                adapter.setDataSrc(it)
                adapter.notifyDataSetChanged()
            } else {
                tv_no_data.visibility = View.VISIBLE
            }
        })

        viewModel.isRefreshing.observe(viewLifecycleOwner, Observer { swipe_refresh.isRefreshing = it })
    }

    override fun setupListeners() {
        super.setupListeners()
        View.OnClickListener { buttonView ->
            when (buttonView.id) {
                R.id.btn_menu -> { viewModel.toastData.value = "do something else..."}
            }
        }.also {
            btn_menu.setOnClickListener(it)
        }

        swipe_refresh.setOnRefreshListener { viewModel.getExhibitionCenter() }
    }

    override fun initSettings() {
        btn_menu.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_menu, 0, 0, 0)
        activity?.also { activity ->
            LinearLayoutManager(activity).also { layoutManager ->
                layoutManager.orientation = LinearLayoutManager.VERTICAL
                rv_ExhibitionCenter.layoutManager = layoutManager
            }
        }

        adapter = ExhibitionCenterAdapter()
        adapter.setItemListener(object : OnItemClickListener {
            override fun onItemClick(item: Any) {
                findNavController().navigate(R.id.action_homeFragment_to_detailFragment,
                    InfoFragment.createBundle(item as ExhibitionCenterItem))
            }
        })

        rv_ExhibitionCenter.adapter = adapter

        viewModel.getExhibitionCenter()

        swipe_refresh.setProgressBackgroundColorSchemeColor(requireContext().getColor(R.color.color_white_1))
        swipe_refresh.setColorSchemeColors(requireContext().getColor(R.color.color_green_1))
    }
}