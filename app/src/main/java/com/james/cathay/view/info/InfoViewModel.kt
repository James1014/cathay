package com.james.cathay.view.info

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.james.cathay.R
import com.james.cathay.model.api.ApiResult
import com.james.cathay.model.api.ApiResult.Companion.loaded
import com.james.cathay.model.api.ApiResult.Companion.loading
import com.james.cathay.model.api.ApiResult.Companion.success
import com.james.cathay.model.vo.PlantItem
import com.james.cathay.view.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.HttpException

class InfoViewModel : BaseViewModel() {
    private val _data = MutableLiveData<List<PlantItem>>()
    val data: LiveData<List<PlantItem>> = _data

    val isRefreshing = MutableLiveData<Boolean>().also { it.value = false }

    // todo: for Demo...
    companion object { const val RID = "f18de02f-b6c9-47c0-8cda-50efad621c14" }

    @ExperimentalCoroutinesApi
    fun getPlant() {
        isRefreshing.value = true
        viewModelScope.launch {
            flow {
                val result =
                    apiRepository.getPlanet(RID)
                if (!result.isSuccessful) throw HttpException(result)
                emit(success(result.body()))
            }
                .flowOn(Dispatchers.IO)
                .onStart { emit(loading()) }
                .catch { e -> emit(ApiResult.error(e)) }
                .onCompletion {
                    emit(loaded())
                    isRefreshing.value = false
                }
                .collect {
                    when (it) {
                        is ApiResult.Success -> _data.value = it.result.result?.results
                        is Error -> toastData.value = app.getString(R.string.api_failed_msg)
                        is ApiResult.Loading -> {
                            // todo: when without SwipeRefreshLayout's progress...
                            // setShowProgress(true)
                        }
                        is ApiResult.Loaded -> {
                            // todo: when without SwipeRefreshLayout's progress...
                            // setShowProgress(false)
                        }
                    }
                }
        }
    }
}