package com.james.cathay.view.detail

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.james.cathay.R
import com.james.cathay.model.vo.PlantItem
import com.james.cathay.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.layout_title.*
import org.koin.android.viewmodel.ext.android.viewModel

class DetailFragment : BaseFragment<DetailViewModel>() {
    private val viewModel by viewModel<DetailViewModel>()
    private lateinit var plantItem: PlantItem

    companion object {
        private const val KEY_PLANT_DATA = "KEY_PLANT_DATA"

        fun createBundle(item: PlantItem): Bundle {
            return Bundle().also {
                it.putSerializable(KEY_PLANT_DATA, item)
            }
        }
    }

    override fun fetchViewModel(): DetailViewModel? { return viewModel }

    override fun getLayoutId(): Int { return R.layout.fragment_detail }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSettings()
    }

    override fun setupListeners() {
        super.setupListeners()
        View.OnClickListener { buttonView ->
            when(buttonView.id) {
                R.id.btn_menu -> findNavController().navigateUp()
            }
        }.also {
            btn_menu.setOnClickListener(it)
        }
    }

    override fun initSettings() {
        arguments?.let {
            plantItem = it.getSerializable(KEY_PLANT_DATA) as PlantItem
            Glide.with(requireContext())
                .load(plantItem.fPic01URL)
                .into(iv_photo)
            btn_menu.text = plantItem.fNameCh
            tv_name_ch.text = plantItem.fNameCh
            tv_name_en.text = plantItem.fNameEn
            tv_also_known.text = plantItem.fAlsoKnown
            tv_brief.text = plantItem.fBrief
            tv_feature.text = plantItem.fFeature
            tv_function.text = plantItem.fFunctionApplication
            tv_update.text = plantItem.fUpdate
        }
    }
}