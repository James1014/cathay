package com.james.cathay.view.info

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.james.cathay.R
import com.james.cathay.model.vo.ExhibitionCenterItem
import com.james.cathay.model.vo.PlantItem
import com.james.cathay.view.base.BaseFragment
import com.james.cathay.view.detail.DetailFragment
import com.james.cathay.view.listener.OnItemClickListener
import kotlinx.android.synthetic.main.fragment_detail.iv_photo
import kotlinx.android.synthetic.main.fragment_info.*
import kotlinx.android.synthetic.main.layout_title.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class InfoFragment : BaseFragment<InfoViewModel>() {
    private val viewModel by viewModel<InfoViewModel>()

    private lateinit var exhibitionCenterItem: ExhibitionCenterItem
    private lateinit var adapter: PlantAdapter

    companion object {
        private const val KEY_CENTER_DATA = "KEY_CENTER_DATA"

        fun createBundle(item: ExhibitionCenterItem): Bundle {
            return Bundle().also {
                it.putSerializable(KEY_CENTER_DATA, item)
            }
        }
    }

    override fun fetchViewModel(): InfoViewModel? { return viewModel }

    override fun getLayoutId(): Int { return R.layout.fragment_info }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().window.statusBarColor = Color.TRANSPARENT

        initSettings()
    }

    override fun initSettings() {
        arguments?.let {
            exhibitionCenterItem = it.getSerializable(KEY_CENTER_DATA) as ExhibitionCenterItem
            btn_menu.text = exhibitionCenterItem.eName
            Glide.with(requireContext())
                .load(exhibitionCenterItem.ePicURL)
                .into(iv_photo)

            tv_info.text = exhibitionCenterItem.eInfo
            tv_category.text = exhibitionCenterItem.eCategory

            when(exhibitionCenterItem.eMemo) {
                "" -> {
                    tv_memo.text = getString(R.string.home_without_close_day)
                    tv_memo.setTextColor(requireContext().getColor(R.color.color_green_1))
                }
                else -> {
                    tv_memo.text = exhibitionCenterItem.eMemo
                    tv_memo.setTextColor(requireContext().getColor(R.color.color_red_1))
                }
            }
        }

        activity?.also { activity ->
            LinearLayoutManager(activity).also { layoutManager ->
                layoutManager.orientation = LinearLayoutManager.VERTICAL
                rv_planet.layoutManager = layoutManager
            }
        }

        adapter = PlantAdapter()
        adapter.setItemListener(object : OnItemClickListener {
            override fun onItemClick(item: Any) {
                findNavController().navigate(R.id.action_infoFragment_to_detailFragment,
                    DetailFragment.createBundle(item as PlantItem))
            }
        })

        rv_planet.adapter = adapter

        viewModel.getPlant()
    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.data.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                adapter.setDataSrc(it)
                adapter.notifyDataSetChanged()
            } else {
                viewModel.toastData.value = "no data..."
            }
        })
    }

    override fun setupListeners() {
        super.setupListeners()

        View.OnClickListener { buttonView ->
            when(buttonView.id) {
                R.id.btn_menu -> findNavController().navigateUp()
                R.id.btn_open_web -> {
                    val openURL = Intent(Intent.ACTION_VIEW)
                    openURL.data = Uri.parse(exhibitionCenterItem.eURL)
                    startActivity(openURL)
                }
            }
        }.also {
            btn_menu.setOnClickListener(it)
            btn_open_web.setOnClickListener(it)
        }
    }
}