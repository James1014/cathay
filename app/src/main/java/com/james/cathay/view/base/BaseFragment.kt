package com.james.cathay.view.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.james.cathay.view.main.MainViewModel
import androidx.lifecycle.ViewModelProviders
import com.james.cathay.utils.GeneralUtils
import com.kaopiz.kprogresshud.KProgressHUD

abstract class BaseFragment<out VM : BaseViewModel> : Fragment() {
    open var mainViewModel: MainViewModel? = null
    var progressHUD: KProgressHUD? = null

    abstract fun fetchViewModel(): VM?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let {
            mainViewModel = ViewModelProviders.of(it).get(MainViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressHUD = KProgressHUD.create(context)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)

        fetchViewModel()?.also { viewModel ->
            viewModel.showProgress.observe(viewLifecycleOwner, Observer {
                if (it) {
                    progressHUD?.show()
                } else {
                    progressHUD?.dismiss()
                }
            })

            viewModel.toastData.observe(viewLifecycleOwner, Observer {
                GeneralUtils.showToast(requireContext(), it)
            })
        }
        setupListeners()
        setupObservers()
    }

    abstract fun getLayoutId(): Int

    abstract fun initSettings()

    open fun setupObservers() {}
    open fun setupListeners() {}
}
