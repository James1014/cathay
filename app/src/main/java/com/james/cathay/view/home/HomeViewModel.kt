package com.james.cathay.view.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.james.cathay.R
import com.james.cathay.model.api.ApiResult
import com.james.cathay.model.api.ApiResult.*
import com.james.cathay.model.api.ApiResult.Companion.loaded
import com.james.cathay.model.api.ApiResult.Companion.loading
import com.james.cathay.model.api.ApiResult.Companion.success
import com.james.cathay.model.vo.ExhibitionCenterItem
import com.james.cathay.view.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.HttpException

class HomeViewModel : BaseViewModel() {
    private val _data = MutableLiveData<List<ExhibitionCenterItem>>()
    val data: LiveData<List<ExhibitionCenterItem>> = _data

    val isRefreshing = MutableLiveData<Boolean>().also { it.value = false }

    // todo: for Demo...
    companion object { const val RID = "5a0e5fbb-72f8-41c6-908e-2fb25eff9b8a" }

    @ExperimentalCoroutinesApi
    fun getExhibitionCenter() {
        isRefreshing.value = true
        viewModelScope.launch {
            flow {
                val result =
                    apiRepository.getExhibitionCenter(RID)
                if (!result.isSuccessful) throw HttpException(result)
                emit(success(result.body()))
            }
                .flowOn(Dispatchers.IO)
                .onStart { emit(loading()) }
                .catch { e -> emit(ApiResult.error(e)) }
                .onCompletion {
                    emit(loaded())
                    isRefreshing.value = false
                }
                .collect {
                    when (it) {
                        is Success -> _data.value = it.result.result?.results
                        is Error -> toastData.value = app.getString(R.string.api_failed_msg)
                        is Loading -> {
                            // todo: when without SwipeRefreshLayout's progress...
                            // setShowProgress(true)
                        }
                        is Loaded -> {
                            // todo: when without SwipeRefreshLayout's progress...
                            // setShowProgress(false)
                        }
                    }
                }
        }
    }
}