package com.james.cathay.view.splash

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.james.cathay.R
import com.james.cathay.view.base.BaseFragment
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class SplashFragment : BaseFragment<SplashViewModel>() {
    private val viewModel by viewModel<SplashViewModel>()

    override fun fetchViewModel(): SplashViewModel? { return viewModel }

    override fun getLayoutId(): Int { return R.layout.fragment_splash }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initSettings()
    }

    override fun initSettings() {
        lifecycleScope.launch {
            delay(3000L)
            // todo: can add some different animation...
            Navigation.findNavController(requireView())
                .navigate(R.id.action_splashFragment_to_homeFragment)
        }
    }
}