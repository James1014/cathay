package com.james.cathay.view.info

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.james.cathay.R
import com.james.cathay.model.vo.PlantItem
import com.james.cathay.view.listener.OnItemClickListener

class PlantAdapter : RecyclerView.Adapter<PlanetViewHolder>() {

    private var mDataSrc: List<PlantItem> = listOf()
    private var itemListener: OnItemClickListener? = null

    fun setDataSrc(list: List<PlantItem>) { mDataSrc = list }

    fun setItemListener(listener: OnItemClickListener) { itemListener = listener }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlanetViewHolder {
        return PlanetViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_plant, parent, false)
        )
    }

    override fun getItemCount(): Int { return mDataSrc.size }

    override fun onBindViewHolder(holder: PlanetViewHolder, position: Int) {
        val plantItem = mDataSrc[position]

        Glide.with(holder.ivPhoto.context)
            .load(plantItem.fPic01URL)
            .into(holder.ivPhoto)

        holder.tvName.text = plantItem.fNameCh
        holder.tvFeature.text = plantItem.fFeature
        holder.itemView.setOnClickListener { itemListener?.onItemClick(plantItem) }

    }
}