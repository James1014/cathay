package com.james.cathay.view.base

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.james.cathay.model.ApiRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

abstract class BaseViewModel : ViewModel(), KoinComponent {
    val app: Application by inject()
    val apiRepository: ApiRepository by inject()

    val toastData = MutableLiveData<String>()

    private val _showProgress by lazy { MutableLiveData<Boolean>() }
    val showProgress: LiveData<Boolean> get() = _showProgress

    // todo: when without SwipeRefreshLayout's progress...
//    fun setShowProgress(show: Boolean) {
//        _showProgress.value = show
//    }
}