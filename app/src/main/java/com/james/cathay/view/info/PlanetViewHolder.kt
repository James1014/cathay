package com.james.cathay.view.info

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.james.cathay.R

class PlanetViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val ivPhoto = itemView.findViewById(R.id.iv_photo) as ImageView
    val tvName = itemView.findViewById(R.id.tv_name) as TextView
    val tvFeature = itemView.findViewById(R.id.tv_feature) as TextView
}
