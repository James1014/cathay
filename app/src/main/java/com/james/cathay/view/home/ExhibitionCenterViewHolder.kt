package com.james.cathay.view.home

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.james.cathay.R

class ExhibitionCenterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val ivPhoto = itemView.findViewById(R.id.iv_photo) as ImageView
    val tvName = itemView.findViewById(R.id.tv_name) as TextView
    val tvInfo = itemView.findViewById(R.id.tv_info) as TextView
    val tvMemo = itemView.findViewById(R.id.tv_memo) as TextView
}