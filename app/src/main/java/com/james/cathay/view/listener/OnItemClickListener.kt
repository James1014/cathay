package com.james.cathay.view.listener

interface OnItemClickListener {
    fun onItemClick(item: Any)
}