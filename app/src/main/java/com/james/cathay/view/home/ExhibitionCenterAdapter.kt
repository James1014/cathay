package com.james.cathay.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.james.cathay.R
import com.james.cathay.model.vo.ExhibitionCenterItem
import com.james.cathay.view.listener.OnItemClickListener

class ExhibitionCenterAdapter : RecyclerView.Adapter<ExhibitionCenterViewHolder>() {

    private var mDataSrc: List<ExhibitionCenterItem> = listOf()
    private var itemListener: OnItemClickListener? = null

    fun setDataSrc(list: List<ExhibitionCenterItem>) { mDataSrc = list }

    fun setItemListener(listener: OnItemClickListener) { itemListener = listener }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExhibitionCenterViewHolder {
        return ExhibitionCenterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_exhibition_center, parent, false)
        )
    }

    override fun getItemCount(): Int { return mDataSrc.size }

    override fun onBindViewHolder(holder: ExhibitionCenterViewHolder, position: Int) {
        val exhibitionCenterItem = mDataSrc[position]

        Glide.with(holder.ivPhoto.context)
            .load(exhibitionCenterItem.ePicURL)
            .into(holder.ivPhoto)

        holder.tvName.text = exhibitionCenterItem.eName
        holder.tvInfo.text = exhibitionCenterItem.eInfo

        when(exhibitionCenterItem.eMemo) {
            "" -> {
                holder.tvMemo.text = holder.tvMemo.context.getString(R.string.home_without_close_day)
                holder.tvMemo.setTextColor(holder.tvMemo.context.getColor(R.color.color_green_1))
            }
            else -> {
                holder.tvMemo.text = exhibitionCenterItem.eMemo
                holder.tvMemo.setTextColor(holder.tvMemo.context.getColor(R.color.color_red_1))
            }
        }

        holder.itemView.setOnClickListener { itemListener?.onItemClick(exhibitionCenterItem) }

    }
}