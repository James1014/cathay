package com.james.cathay

import android.app.Application
import com.james.cathay.di.apiModule
import com.james.cathay.di.appModule
import com.james.cathay.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        val module = listOf(
            appModule,
            apiModule,
            viewModelModule
        )

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(module)
        }
    }
}