# Cathay

#### This project only for Cathay demo.

## Architecture: `MVVM`

## A collection of samples using the Architecture Components:
- Coroutines
- Koin
- Lifecycle
- LiveData
- Navigation
- OKHttp
- Retrofit2
- ViewModel
